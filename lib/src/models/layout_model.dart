import 'package:diseno_app/src/pages/slideshow_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class LayoutModel with ChangeNotifier{
  Widget _currentPage = SlideShowPage();
  set currentPage(Widget page){
    this._currentPage = page;
    notifyListeners();
  }

  Widget get currentPage => this._currentPage;
} 
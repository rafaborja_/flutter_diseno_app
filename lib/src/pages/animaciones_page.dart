import 'package:flutter/material.dart';
import 'dart:math';
class AnimacionesPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CuadradoAnimado(),
      ),
    );
  }
}

class CuadradoAnimado extends StatefulWidget {

  @override
  _CuadradoAnimadoState createState() => _CuadradoAnimadoState();
}

class _CuadradoAnimadoState extends State<CuadradoAnimado> with SingleTickerProviderStateMixin{
  AnimationController controller;
  Animation<double> rotacion;

  @override
  void initState() {
    //EL controlador de la animación 
    controller = new AnimationController(vsync: this, duration: Duration(milliseconds: 4000));
    //LA animacion, controlada por el controlller
    rotacion = Tween(begin: 0.0, end: 2.0 * pi).animate(
      //Para aplicarle una curva a la animación
      CurvedAnimation(parent: controller, curve: Curves.bounceIn));

    //Se agrega un evento de escucha al inicio del Stateful para que esté pendiente del estado de la animación
    controller.addListener(() {
      print(controller.status);
      //Si la animación se completa, que se resetee
      if(controller.status == AnimationStatus.completed){
      controller.reverse();
    } /*else if(controller.status == AnimationStatus.dismissed){
      controller.forward();
    } */
    });
    //Iniciamos la animación al iniciar el widget
    controller.forward();
    super.initState();
  }

  @override
  void dispose() { 
    //PAra el controller deje de escuchar cambios al terminar la animación
    controller.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    //Decirle a la animación qué va a hacer cuando se construya el widget
    controller.repeat();
    //Colocamos el objeto a animar, ddentro de un animatedbuilder para aplicarle la animación
    return AnimatedBuilder(
      //Le especificamos la animación
      animation: rotacion,
      //NUestro objeto
      child: Rectangulo(),
      //Para cuando se comience a construir la animación
      builder: (BuildContext context, Widget child) {
        //Ya que es una rotación, envolvemos el objeto en un transform.rotate()
        return Transform.rotate(
          //Decimos que el angulo va a ser el valor actual de la animación
          angle: rotacion.value,
          //Y el objeto como child
          child: child);
      },
    );
  }
}

class Rectangulo extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
    return Container(
       width: 70,
       height: 70,
       decoration: BoxDecoration(
         color: Colors.blue
       ),
     );
   }
}
import 'package:diseno_app/src/pages/theme/theme_changer.dart';
import 'package:diseno_app/src/routes/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';


class LauncherPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Diseños en Flutter Telefono'),
      ),
      body: _ListaOpciones(),
      drawer: _MenuPrincipal(),
   );
  }
}
class _ListaOpciones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return ListView.separated(
      physics: BouncingScrollPhysics(),
      separatorBuilder: (context,i)=>Divider(color: appTheme.primaryColorLight), 
      itemCount: pageRoutes.length,
      itemBuilder: (context,i)=>ListTile(
        leading: FaIcon(pageRoutes[i].icon, color: appTheme.accentColor),
        title: Text(pageRoutes[i].titulo),
        trailing: Icon(Icons.chevron_right, color: appTheme.accentColor),
        onTap: (){
          Navigator.push(context, CupertinoPageRoute(builder: (BuildContext context) => pageRoutes[i].page));
        },
      )
      );
  }
}

class _MenuPrincipal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    return Drawer(
      child: Container(
        child: Column(
          children: [
            SafeArea(
              child: Container(
                padding: EdgeInsets.all(10),
                width: double.infinity,
                height: 200,
                child: CircleAvatar(
                  backgroundColor: appTheme.currentTheme.accentColor,
                  child: Text('AB', style: TextStyle(fontSize: 50, color: appTheme.currentTheme.primaryColorLight))),
              ),
            ),
            Expanded(child: _ListaOpciones()),
            ListTile(
              leading: Icon(Icons.lightbulb_outline, color: appTheme.currentTheme.accentColor),
              title: Text('Dark Mode'),
              trailing: Switch.adaptive(value: appTheme.darkTheme, activeColor: appTheme.currentTheme.accentColor, onChanged: (value) => appTheme.darkTheme= value),
            ),
            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen, color: appTheme.currentTheme.accentColor),
                title: Text('Custom Theme'),
                trailing: Switch.adaptive(value: appTheme.customTheme, activeColor: appTheme.currentTheme.accentColor, onChanged: (value) => appTheme.customTheme = value),
              ),
            )
          ],
        )
      ),
    );
  }
}
import 'package:diseno_app/src/pages/theme/theme_changer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class SliverListPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _MainScroll(),
          Positioned(
            bottom: -10,
            right: 0,
            child: _BotonNewList()
          )
        ],
      ),
   );
  }
}

class _MainScroll extends StatelessWidget {
  final items = [
    _ListItem( 'Orange', Color(0xffF08F66) ),
    _ListItem( 'Family', Color(0xffF2A38A) ),
    _ListItem( 'Subscriptions', Color(0xffF7CDD5) ),
    _ListItem( 'Books', Color(0xffFCEBAF) ),
    _ListItem( 'Orange', Color(0xffF08F66) ),
    _ListItem( 'Family', Color(0xffF2A38A) ),
    _ListItem( 'Subscriptions', Color(0xffF7CDD5) ),
    _ListItem( 'Books', Color(0xffFCEBAF) ),
    _ListItem( 'Orange', Color(0xffF08F66) ),
    _ListItem( 'Family', Color(0xffF2A38A) ),
    _ListItem( 'Subscriptions', Color(0xffF7CDD5) ),
    _ListItem( 'Books', Color(0xffFCEBAF) ),
    _ListItem( 'Orange', Color(0xffF08F66) ),
    _ListItem( 'Family', Color(0xffF2A38A) ),
    _ListItem( 'Subscriptions', Color(0xffF7CDD5) ),
    _ListItem( 'Books', Color(0xffFCEBAF) ),
  ];
  
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        // Aca se crea el header personalizado
        SliverPersistentHeader(
          // El floating es para que justo cuando se deslize hacia arriba aparezca
          floating: true,
          delegate: _SliverCustomHeaderDelegate(
            // El minheight es lo minimo que se va a estirar y el maxheight en viceversa.
            minheight: 200,
            maxheight: 250,
            // El container es para que el las muy letras no queden encima de la lista
            child: Container(
              alignment: Alignment.centerLeft,
              color: appTheme.scaffoldBackgroundColor,
              child: _Titulo(),
            ))
          ),
          SliverList(
            delegate: SliverChildListDelegate(items))
      ],
    );
  }
}

// Hereda las funciones de SliverPersistentHeaderDelegate
class _SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate{
  final double minheight;
  final double maxheight;
  final Widget child;
  _SliverCustomHeaderDelegate({@required this.minheight, @required this.maxheight, @required this.child});
  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return SizedBox.expand(child: child);
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => maxheight;

  @override
  // TODO: implement minExtent
  double get minExtent => minheight;

  @override
  bool shouldRebuild(_SliverCustomHeaderDelegate oldDelegate) {
    return maxheight != oldDelegate.maxheight ||
           minheight != oldDelegate.minheight ||
           child != oldDelegate.child;
  }

}

class _Titulo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    return Column(
      children: <Widget>[
        SizedBox(height: 30),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          child: Text('New', style: TextStyle(color: (appTheme.darkTheme) ? Colors.grey : Color(0xff532128), fontSize: 50))
        ),
        Stack(
          children: <Widget>[
            SizedBox(width: 100),
            Positioned(
              bottom: 8,
              child: Container(
                width: 130,
                height: 8,
                decoration: BoxDecoration(
                  color: (appTheme.darkTheme) ? Colors.grey : Color(0xffF7CDD5)
                ),
              ),
            ),
            Container(
              child: Text('List',style: TextStyle(color: Color(0xffD93A30), fontSize: 50, fontWeight: FontWeight.bold))
            )
          ],
        )
      ],
    );
  }
}

class _ListItem extends StatelessWidget {
  final String titulo;
  final Color color;
  _ListItem(this.titulo, this.color);
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    return Container(
      child: Text(titulo,style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20)),
      alignment: Alignment.centerLeft,
      height: 130,
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(30),
      decoration: BoxDecoration(
        color: (appTheme.darkTheme) ? Colors.grey : color,
        borderRadius: BorderRadius.circular(30)
      ),
    );
  }
}

class _BotonNewList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final size = MediaQuery.of(context).size;
    return ButtonTheme(
      height: 100,
      minWidth: size.width * 0.9,
      child: RaisedButton(
        onPressed: (){},
        color: (appTheme.darkTheme) ? appTheme.currentTheme.accentColor : Color(0xffED6762),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(50))),
        child: Text(
          'CREATE NEW LIST',
          style: TextStyle(
            color: appTheme.currentTheme.scaffoldBackgroundColor,
            fontSize: 18,
            fontWeight: FontWeight.bold,
            letterSpacing: 3
          ),
        ),)
    );
  }
}
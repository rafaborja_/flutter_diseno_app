import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HeaderCuadrado extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      color: Color(0xff615AAB)
    );
  }
}

class HeaderBordesRedondeados extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      //color: Color(0xff615AAB),
      decoration: BoxDecoration(
        color: Color(0xff615AAB),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(70),
          bottomRight: Radius.circular(70))),
    );
  }

}

class HeaderDiagonal extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
      painter: _diagonalPainter(),
    )
    );
  }
}

class _diagonalPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    //Lapiz
    final pen = Paint();
    //Color del lapiz
    pen.color = Color(0xff615AAB);
    //Estilo de trazo. Llenar o solo lineas
    pen.style = PaintingStyle.fill;
    //Ancho de la brocha
    pen.strokeWidth = 20;
    //El path, que funciona para mover el lapiz
    final path = Path();
    path.lineTo(0, size.height * 0.35);
    path.lineTo(size.width, size.height * 0.30);
    path.lineTo(size.width, 0);
    //Le decimos que se va a dibujar el movimiento del path con el lápiz
    canvas.drawPath(path, pen);
  }

  @override
  bool shouldRepaint(_diagonalPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(_diagonalPainter oldDelegate) => false;
}

class HeaderTriangular extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
      painter: _triangularPainter(),
    )
    );
  }
}

class _triangularPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    //Lapiz
    final pen = Paint();
    //Color del lapiz
    pen.color = Color(0xff615AAB);
    //Estilo de trazo. Llenar o solo lineas
    pen.style = PaintingStyle.fill;
    //Ancho de la brocha
    pen.strokeWidth = 20;
    //El path, que funciona para mover el lapiz
    final path = Path();
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    //path.lineTo(size.width, 0);
    //Le decimos que se va a dibujar el movimiento del path con el lápiz
    canvas.drawPath(path, pen);
  }

  @override
  bool shouldRepaint(_triangularPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(_triangularPainter oldDelegate) => false;
}

class HeaderPico extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
      painter: _picoPainter(),
    )
    );
  }
}

class _picoPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    //Lapiz
    final pen = Paint();
    //Color del lapiz
    pen.color = Color(0xff615AAB);
    //Estilo de trazo. Llenar o solo lineas
    pen.style = PaintingStyle.fill;
    //Ancho de la brocha
    pen.strokeWidth = 20;
    //El path, que funciona para mover el lapiz
    final path = Path();
    path.lineTo(0, size.height * 0.25);
    path.lineTo(size.width * 0.5, size.height * 0.32);
    path.lineTo(size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    //path.lineTo(size.width, 0);
    //Le decimos que se va a dibujar el movimiento del path con el lápiz
    canvas.drawPath(path, pen);
  }

  @override
  bool shouldRepaint(_picoPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(_picoPainter oldDelegate) => false;
}

class HeaderCurvo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
      painter: _curvoPainter(),
    )
    );
  }
}

class _curvoPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    //Lapiz
    final pen = Paint();
    //Color del lapiz
    pen.color = Color(0xff615AAB);
    //Estilo de trazo. Llenar o solo lineas
    pen.style = PaintingStyle.fill;
    //Ancho de la brocha
    pen.strokeWidth = 20;
    //El path, que funciona para mover el lapiz
    final path = Path();
    path.lineTo(0, size.height * 0.25);
    path.quadraticBezierTo(size.width * 0.5, size.height * 0.40, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    //Le decimos que se va a dibujar el movimiento del path con el lápiz
    canvas.drawPath(path, pen);
  }

  @override
  bool shouldRepaint(_curvoPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(_curvoPainter oldDelegate) => false;
}

class HeaderWave extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: CustomPaint(
      painter: _wavePainter(),
    )
    );
  }
}

class _wavePainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    //Gradiente
    final gradiente = LinearGradient(colors: [
      Color(0xff6D05E8),
      Color(0xffC012FF),
      Color(0xff6D05FA)
    ],
    stops: [
      0.0,
      0.5,
      1.0
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter);
    //Rect
    final rect = Rect.fromCircle(
      center: Offset(size.width * 0.5, 100),
      radius : 180,
    );
    //Lapiz
    final pen = Paint();
    pen.shader = gradiente.createShader(rect);
    //Color del lapiz
    // pen.color = Color(0xff615AAB);
    //Estilo de trazo. Llenar o solo lineas
    pen.style = PaintingStyle.fill;
    //Ancho de la brocha
    pen.strokeWidth = 20;
    //El path, que funciona para mover el lapiz
    final path = Path();
    path.lineTo(0, size.height * 0.25);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.30, size.width * 0.5, size.height * 0.25);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.20, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    //Le decimos que se va a dibujar el movimiento del path con el lápiz
    canvas.drawPath(path, pen);
  }

  @override
  bool shouldRepaint(_wavePainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(_wavePainter oldDelegate) => false;
}

class IconHeader extends StatelessWidget {
  final IconData icon;
  final String titulo;
  final String subtitulo;
  final Color color1;
  final Color color2;

  IconHeader({
    @required this.icon, 
    @required this.titulo, 
    @required this.subtitulo, 
    this.color1 = Colors.grey, 
    this.color2 = Colors.blueGrey});
  @override
  Widget build(BuildContext context) {
    final Color colorBlanco = Colors.white.withOpacity(0.7);
    return Stack(
      children: <Widget>[
        _IconHeaderBackground(color1,color2),
        Positioned(
          top: -50,
          left: -70,
          child: FaIcon(icon,size:250,color:Colors.white.withOpacity(0.2))
        ),
        Column(
          children: <Widget>[
            SizedBox(height: 80, width: double.infinity),
            Text(subtitulo, style: TextStyle(fontSize: 20,color: colorBlanco)),
            SizedBox(height: 20),
            Text(titulo, style: TextStyle(fontSize: 25,color: colorBlanco, fontWeight: FontWeight.bold)),
            SizedBox(height: 20),
            FaIcon(icon,size:80,color:Colors.white)
          ],
        )
        ],
    );
  }
}

class _IconHeaderBackground extends StatelessWidget {
  final Color color1;
  final Color color2;
  _IconHeaderBackground(this.color1, this.color2);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(80)),
        gradient: LinearGradient(
          colors: [
            color1,
            color2
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter)
      ),
    );
  }
}
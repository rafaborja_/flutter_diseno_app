import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
class RadialProgress extends StatefulWidget {
  final double porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final double grosorPrimario;
  final double grosorSecundario;
  RadialProgress({
    @required this.porcentaje, 
    this.colorPrimario = Colors.indigo, 
    this.colorSecundario = Colors.grey,
    this.grosorPrimario = 10,
    this.grosorSecundario = 4});

  @override
  _RadialProgressState createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress> with SingleTickerProviderStateMixin{
  AnimationController controller;
  double porcentajeAnterior;
  @override
  void initState() {
    controller = new AnimationController(
      vsync: this, duration: Duration(milliseconds: 200));
    porcentajeAnterior = widget.porcentaje;
    super.initState();
  }
  @override
  void dispose() { 
    controller.dispose();
    super.dispose();
  }
    
  @override
  Widget build(BuildContext context) {
    controller.forward(from: 0.0);
    double posicion;
    final diferencia = widget.porcentaje - porcentajeAnterior;
    porcentajeAnterior = widget.porcentaje;
    return AnimatedBuilder(
      animation: controller,
      builder: (BuildContext context, Widget child) {
        posicion = widget.porcentaje - diferencia + (diferencia * controller.value);
        // print("Anterior $porcentajeAnterior, Actual ${widget.porcentaje}, Diferencia $diferencia, Posición $posicion");
        return Container(
         width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(10),
          child: CustomPaint(
            painter: _MiRadialProgress(
            posicion, 
            widget.colorPrimario, 
            widget.colorSecundario,
            widget.grosorPrimario,
            widget.grosorSecundario),
          ),
         );
      },
    );
  }

}

class _MiRadialProgress extends CustomPainter {
  final porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final double grosorPrimario;
  final double grosorSecundario;
  _MiRadialProgress(
    this.porcentaje, 
    this.colorPrimario, 
    this.colorSecundario,
    this.grosorPrimario,
    this.grosorSecundario);
  @override
  void paint(Canvas canvas, Size size) {
    //Circulo Completado
    final paint = new Paint()
      ..strokeWidth = grosorSecundario
      ..color = colorSecundario
      ..style = PaintingStyle.stroke;
    Offset center = new Offset(size.width * 0.5, size.height * 0.5);
    double radio = min(size.width * 0.5, size.height * 0.5);
    canvas.drawCircle(center, radio, paint);
    //Arco
    final paintArco = new Paint()
      ..strokeWidth = grosorPrimario
      ..color = colorPrimario
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    //Parte que se deberá ir llenando
    double arcAngle = 2 * pi * (porcentaje / 100);
    canvas.drawArc(Rect.fromCircle(center: center, radius: radio), -pi / 2,
        arcAngle, false, paintArco);
  }

  @override
  bool shouldRepaint(_MiRadialProgress oldDelegate) => true;
  }
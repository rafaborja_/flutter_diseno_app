import 'package:diseno_app/src/models/slider_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

List<Widget>globalSlides = [];
class SlideShow extends StatelessWidget {
final List<Widget> slides;
final bool topDots;
final Color colorPrimario;
final Color colorSecundario;
final double primaryBullet;
final double secondaryBullet;
SlideShow({
  @required this.slides,
  this.colorPrimario = Colors.blue,
  this.colorSecundario = Colors.grey,
  this.topDots = false,
  this.primaryBullet = 12,
  this.secondaryBullet = 12});
  @override
  Widget build(BuildContext context) {
    globalSlides = slides;
    return ChangeNotifierProvider(
      create: (_) => new SliderModel(),
      child: SafeArea(
          child: Center(
            child: Column(
              children: <Widget>[
                if (this.topDots == true) _Dots(colorPrimario, colorSecundario, primaryBullet, secondaryBullet), 
                Expanded(child: _Slides()),                
                if (topDots == false) _Dots(colorPrimario, colorSecundario, primaryBullet, secondaryBullet)
                ],
            ),
          ),
      )
    );
  }
}

class _Dots extends StatelessWidget {
final Color colorPrimario;
final Color colorSecundario;
final double primaryBullet;
final double secondaryBullet;
_Dots(this.colorPrimario, 
  this.colorSecundario,
  this.primaryBullet,
  this.secondaryBullet);
  @override
  Widget build(BuildContext context) {
    List<Widget> listDot = [];
    List.generate(globalSlides.length, (i) => listDot.add(_Dot(i, colorPrimario, colorSecundario, primaryBullet, secondaryBullet)));
    return Container(
      width: double.infinity,
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: listDot,
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;
  final Color colorPrimario;
  final Color colorSecundario;
  final double primaryBullet;
  final double secondaryBullet;
  _Dot(this.index, 
  this.colorPrimario, 
  this.colorSecundario,
  this.primaryBullet,
  this.secondaryBullet);

  @override
  Widget build(BuildContext context) {
    final pageViewIndex = Provider.of<SliderModel>(context).currentpage;
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      width: (pageViewIndex >= index - 0.5 && pageViewIndex < index + 0.5) ? primaryBullet : secondaryBullet,
      height: (pageViewIndex >= index - 0.5 && pageViewIndex < index + 0.5) ? primaryBullet : secondaryBullet,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        color: (pageViewIndex >= index - 0.5 && pageViewIndex < index + 0.5) ? colorPrimario : colorSecundario,
        shape: BoxShape.circle
      ),
    );
  }
}

class _Slides extends StatefulWidget {
  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {
  @override
  Widget build(BuildContext context) {
    final pageViewController = new PageController();

    pageViewController.addListener(() {
      print('Pagina actual: ${pageViewController.page}');
      Provider.of<SliderModel>(context, listen: false).currentpage = pageViewController.page;
    });

    @override
    void dispose() { 
      pageViewController.dispose();
      super.dispose();
    }

    return Container(
      child: PageView(
        controller: pageViewController,
        children: globalSlides,
      ),
    );
  }
}